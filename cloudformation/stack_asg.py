from troposphere import Base64, FindInMap, GetAtt, GetAZs, Join, Output
from troposphere import Parameter, Ref, Template
import troposphere.ec2 as ec2
import troposphere.elasticloadbalancing as elb
from troposphere.cloudformation import Init, InitFile, InitFiles, \
    InitConfig, InitService, InitServices
from troposphere.autoscaling import AutoScalingGroup, Tag
from troposphere.autoscaling import LaunchConfiguration
from troposphere.policies import (
    AutoScalingReplacingUpdate, AutoScalingRollingUpdate, UpdatePolicy
)
from troposphere import autoscaling


def AddAMI(template):
    template.add_mapping("RegionMap", {
        "eu-west-1": {"AMI": "ami-01ccc867"},
        "eu-central-1": {"AMI": "ami-b968bad6"}
    })


def main():
    template = Template()
    template.add_version("2010-09-09")

    template.add_description(
        "AWS CloudFormation Sample Template: ELB with ASG")

    AddAMI(template)

    # Add the Parameters
    ScaleCapacity = template.add_parameter(Parameter(
        "ScaleCapacity",
        Default="1",
        Type="String",
        Description="Number of APP servers to run",
    ))

    keyname_param = template.add_parameter(Parameter(
        "KeyName",
        Type="String",
        Default="manuel",
        Description="Name of an existing EC2 KeyPair to "
                    "enable SSH access to the instance",
    ))

    template.add_parameter(Parameter(
        "InstanceType",
        Type="String",
        Description="WebServer EC2 instance type",
        Default="t2.micro",
        AllowedValues=[
            "t2.micro", "t2.small", "t2.medium",
            ],
        ConstraintDescription="must be a valid EC2 instance type.",
    ))

    webport_param = template.add_parameter(Parameter(
        "WebServerPort",
        Type="String",
        Default="5000",
        Description="TCP/IP port of the web server",
    ))

    docker_image = template.add_parameter(Parameter(
        "DockerImage",
        Type="String",
        Default="chadell/testdevops:latest",
        Description="Docker Image to use",
    ))

    # Define the instance security group
    instance_sg = template.add_resource(
        ec2.SecurityGroup(
            "InstanceSecurityGroup",
            GroupDescription="Enable SSH and HTTP access on the inbound port",
            SecurityGroupIngress=[
                ec2.SecurityGroupRule(
                    IpProtocol="tcp",
                    FromPort="22",
                    ToPort="22",
                    CidrIp="0.0.0.0/0",
                ),
                ec2.SecurityGroupRule(
                    IpProtocol="tcp",
                    FromPort=Ref(webport_param),
                    ToPort=Ref(webport_param),
                    CidrIp="0.0.0.0/0",
                ),
            ]
        )
    )

    # Add Autoscaling group
    LaunchConfig = template.add_resource(LaunchConfiguration(
        "LaunchConfiguration",
        Metadata=autoscaling.Metadata(
            Init({
                'config': InitConfig(
                    packages={'yum': {'docker': []}},
                    commands={
                        'run_docker': {
                            'command': Join('', [
                                'sudo service docker start; ',
                                'sudo docker run -d -p ',
                                Ref(webport_param),
                                ':5000 ',
                                Ref(docker_image)
                            ])
                        },
                    }
                )})
        ),
        UserData=Base64(Join('', [
            "#!/bin/bash -xe\n",
            "yum update -y aws-cfn-bootstrap\n",
            "/opt/aws/bin/cfn-init -v ",
            "         --stack ", {"Ref": "AWS::StackName"},
            "         --resource LaunchConfiguration ",
            "         --region ", Ref("AWS::Region"), "\n",
            "/opt/aws/bin/cfn-signal -e $? ",
            "         --stack ", {"Ref": "AWS::StackName"},
            "         --resource AutoscalingGroup ",
            "         --region ", Ref("AWS::Region"), "\n",
        ])),
        SecurityGroups=[Ref(instance_sg)],
        KeyName=Ref(keyname_param),
        InstanceType=Ref("InstanceType"),
        ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
    ))

    elasticLB = template.add_resource(elb.LoadBalancer(
        'ElasticLoadBalancer',
        AvailabilityZones=GetAZs(""),
        ConnectionDrainingPolicy=elb.ConnectionDrainingPolicy(
            Enabled=True,
            Timeout=300,
        ),
        CrossZone=True,
        # Instances=[Ref(r) for r in web_instances],
        Listeners=[
            elb.Listener(
                LoadBalancerPort="80",
                InstancePort=Ref(webport_param),
                Protocol="HTTP",
            ),
        ],
        HealthCheck=elb.HealthCheck(
            Target=Join("", ["HTTP:", Ref(webport_param), "/"]),
            HealthyThreshold="3",
            UnhealthyThreshold="5",
            Interval="120",
            Timeout="30",
        )
    ))

    AutoScalingG = template.add_resource(AutoScalingGroup(
        "AutoscalingGroup",
        DesiredCapacity=Ref(ScaleCapacity),
        # Tags=[
        #     Tag("Environment", Ref(EnvType), True)
        # ],
        LaunchConfigurationName=Ref(LaunchConfig),
        MinSize=Ref(ScaleCapacity),
        MaxSize=Ref(ScaleCapacity),
        # VPCZoneIdentifier=[Ref(ApiSubnet1), Ref(ApiSubnet2)],
        LoadBalancerNames=[Ref(elasticLB)],
        AvailabilityZones=GetAZs(""),
        HealthCheckType="EC2",
        UpdatePolicy=UpdatePolicy(
            AutoScalingReplacingUpdate=AutoScalingReplacingUpdate(
                WillReplace=True,
            ),
            AutoScalingRollingUpdate=AutoScalingRollingUpdate(
                PauseTime='PT5M',
                MinInstancesInService="1",
                MaxBatchSize='1',
                WaitOnResourceSignals=True
            )
        )
    ))

    template.add_output(Output(
        "URL",
        Description="URL of devops demo",
        Value=Join("", ["http://", GetAtt(elasticLB, "DNSName")])
    ))

    print(template.to_json())


if __name__ == '__main__':
    main()
